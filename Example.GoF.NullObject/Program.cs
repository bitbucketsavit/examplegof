﻿using System;

namespace Example.GoF.NullObject
{
    /// <summary>
    /// Пример применения шаблона Null Object:
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            AbstractEntity realEntity = new RealEntity();
            realEntity.doSomething(); // RealEntity::doSomething

            AbstractEntity unknownEntity = new NullEntity();
            unknownEntity.doSomething(); // NullEntity::doSomething
        }
    }

    // Define other methods and classes here
    public abstract class AbstractEntity
    {
        public abstract void doSomething();
    }

    public class RealEntity : AbstractEntity
    {
        public override void doSomething()
        {
            Console.WriteLine("RealEntity::doSomething");
        }
    }


    public class NullEntity : AbstractEntity
    {
        public override void doSomething()
        {
            Console.WriteLine("NullEntity::doSomething");
        }
    }
}
