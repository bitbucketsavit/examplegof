﻿using System;
using System.Collections.Generic;

/**
 * 
 * Возможность добавлена с .NET 4.0
 * 
 * */

namespace Example.GoF.ObserverNative
{
    class Program
    {
        static void Main(string[] args)
        {
            // Define a provider and two observers.
            LocationTracker provider = new LocationTracker();

            LocationReporter reporter1 = new LocationReporter("FixedGPS");
            reporter1.Subscribe(provider);

            LocationReporter reporter2 = new LocationReporter("MobileGPS");
            reporter2.Subscribe(provider);


            provider.TrackLocation(new Location(47.6456, -122.1312));
            provider.TrackLocation(new Location(47.6456, -122.1324));
            reporter1.Unsubscribe();
            provider.TrackLocation(new Location(47.6677, -122.1199));
            provider.TrackLocation(null);
            provider.EndTransmission();

            Console.Read();
        }
    }


    public struct Location
    {
        public Location(double latitude, double longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        public double Latitude { get; }

        public double Longitude { get; }
    }


    /**
     * Класс LocationReporter предоставляет реализацию IObserver<T>. 
     * Отображает на консоли данные о текущем расположении. 
     * Его конструктор включает параметр name, с помощью которого экземпляр LocationReporter может идентифицировать себя в строковых выходных данных. 
     * Он также включает метод Subscribe, который упаковывает вызов поставщика метода Subscribe. 
     * Это позволяет методу присваивать возвращаемую ссылку на IDisposable закрытой переменной. 
     * Класс LocationReporter также включает метод Unsubscribe, который вызывает метод IDisposable.Dispose объекта, возвращаемого методом IObservable<T>.Subscribe.
     * */

    public class LocationReporter : IObserver<Location>
    {
        private IDisposable unsubscriber;
        private readonly string instName;

        public LocationReporter(string name)
        {
            this.instName = name;
        }

        public string Name
        { get { return this.instName; } }

        public virtual void Subscribe(IObservable<Location> provider)
        {
            if (provider != null)
                unsubscriber = provider.Subscribe(this);
        }

        public virtual void OnCompleted()
        {
            Console.WriteLine("The Location Tracker has completed transmitting data to {0}.", this.Name);
            this.Unsubscribe();
        }

        public virtual void OnError(Exception e)
        {
            Console.WriteLine("{0}: The location cannot be determined.", this.Name);
        }

        public virtual void OnNext(Location value)
        {
            Console.WriteLine("{2}: The current location is {0}, {1}", value.Latitude, value.Longitude, this.Name);
        }

        public virtual void Unsubscribe()
        {
            unsubscriber.Dispose();
        }
    }

    /**
     * Класс LocationTracker предоставляет реализацию IObservable<T>. 
     * Его методу TrackLocation передается допускающий значение null объект Location, содержащий данные широты и долготы. 
     * Если Location не null, метод TrackLocation вызывает метод OnNext каждого наблюдателя.
     * */

    public class LocationTracker : IObservable<Location>
    {
        public LocationTracker()
        {
            observers = new List<IObserver<Location>>();
        }

        private List<IObserver<Location>> observers;

        public IDisposable Subscribe(IObserver<Location> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }

        private class Unsubscriber : IDisposable
        {
            private List<IObserver<Location>> _observers;
            private readonly IObserver<Location> _observer;

            public Unsubscriber(List<IObserver<Location>> observers, IObserver<Location> observer)
            {
                _observers = observers;
                _observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        public void TrackLocation(Location? loc)
        {
            foreach (var observer in observers)
            {
                if (!loc.HasValue)
                    observer.OnError(new LocationUnknownException());
                else
                    observer.OnNext(loc.Value);
            }
        }

        public void EndTransmission()
        {
            foreach (var observer in observers.ToArray())
                if (observers.Contains(observer))
                    observer.OnCompleted();

            observers.Clear();
        }
    }

    /* 
     * Если значение Location — null, метод TrackLocation создает объект LocationNotFoundException, который показан в следующем примере. 
     * Затем вызывает метод OnError каждого наблюдателя и передает его объекту LocationNotFoundException
     * 
     */

    public class LocationUnknownException : Exception
    {
        internal LocationUnknownException()
        { }
    }
}
