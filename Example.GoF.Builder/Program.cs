﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Строитель (Builder) - шаблон проектирования, который инкапсулирует создание объекта и позволяет разделить его на различные этапы.
 * 
 * Когда использовать паттерн Строитель?
 * Когда процесс создания нового объекта не должен зависеть от того, из каких частей этот объект состоит и как эти части связаны между собой
 * Когда необходимо обеспечить получение различных вариаций объекта в процессе его создания
*/


namespace Example.GoF.Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            // содаем объект пекаря
            Baker baker = new Baker();

            // создаем билдер для ржаного хлеба
            BreadBuilder builder = new RyeBreadBuilder();

            // выпекаем
            baker.Bake(builder);
            Bread ryeBread = builder.Bread;
            Console.WriteLine(ryeBread.ToString());

            // cоздаем билдер для пшеничного хлеба
            builder = new WheatBreadBuilder();
            baker.Bake(builder);
            Bread wheatBread = builder.Bread;
            Console.WriteLine(wheatBread.ToString());

            Console.Read();

        }
    }


    //мука
    class Flour
    {
        // какого сорта мука
        public string Sort { get; set; }
    }
    // соль
    class Salt
    { }
    // пищевые добавки
    class Additives
    {
        public string Name { get; set; }
    }

    class Bread
    {
        // пшеничная мука
        public Flour WheatFlour { get; set; }
        // ржаная мука
        public Flour RyeFlour { get; set; }
        // соль
        public Salt Salt { get; set; }
        // пищевые добавки
        public Additives Additives { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (WheatFlour != null)
                sb.Append("Пшеничная мука " + WheatFlour.Sort + "\n");
            if (RyeFlour != null)
                sb.Append("Ржаная мука " + RyeFlour.Sort + " \n");
            if (Salt != null)
                sb.Append("Соль \n");
            if (Additives != null)
                sb.Append("Добавки: " + Additives.Name + " \n");
            return sb.ToString();
        }
    }


    /// <summary>
    /// абстрактный класс строителя
    /// </summary>
    abstract class BreadBuilder
    {
        public Bread Bread { get; private set; }
        public BreadBuilder()
        {
            Bread = new Bread();
        }
        public abstract void SetWheatFlour();
        public abstract void SetRyeFlour();
        public abstract void SetSalt();
        public abstract void SetAdditives();
    }


    /// <summary>
    /// пекарь
    /// </summary>
    class Baker
    {
        public void Bake(BreadBuilder breadBuilder)
        {
            breadBuilder.SetWheatFlour();
            breadBuilder.SetRyeFlour();
            breadBuilder.SetSalt();
            breadBuilder.SetAdditives();
        }
    }


    /// <summary>
    /// строитель для ржаного хлеба
    /// </summary>
    class RyeBreadBuilder : BreadBuilder
    {
        public override void SetWheatFlour()
        {
            // не используется
        }

        public override void SetRyeFlour()
        {
            this.Bread.RyeFlour = new Flour { Sort = "1 сорт" };
        }

        public override void SetSalt()
        {
            this.Bread.Salt = new Salt();
        }

        public override void SetAdditives()
        {
            // не используется
        }
    }


    /// <summary>
    /// строитель для пшеничного хлеба
    /// </summary>
    class WheatBreadBuilder : BreadBuilder
    {
        public override void SetWheatFlour()
        {
            this.Bread.WheatFlour = new Flour { Sort = "высший сорт" };
        }

        public override void SetRyeFlour()
        {
            // не используется
        }

        public override void SetSalt()
        {
            this.Bread.Salt = new Salt();
        }

        public override void SetAdditives()
        {
            this.Bread.Additives = new Additives { Name = "улучшитель хлебопекарный" };
        }
    }

}
