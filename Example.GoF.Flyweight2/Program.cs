﻿using System;
using System.Collections.Generic;

namespace Example.GoF.Flyweight2
{
    /*
     * Паттерн Приспособленец (Flyweight)
     * 
     * Допустим, мы проектируем программу для моделирования города. Город состоит из отдельных домов, поэтому нам надо создать объекты этих домов. 
     * Однако домов в городе может быть множество: сотни, тысячи. Они могут иметь разный вид, отличаться по различным признакам. Однако, как правило, многие дома делаются по стандартным проектам. 
     * И фактически мы можем выделить несколько типов домов, например, пятиэтажные кирпичные хрущевки, многоэтажные панельные высотки и так далее.
     * 
     * Используя некоторый анализ, мы можем выделить внутренне состояния домов и внешнее. 
     * К внутреннему состоянию, например, может относиться количество этажей, материал (кирпичи, панели и т.д.), или те показатели, которые определены его шаблоном, планом проектирования. 
     * К внешнему состоянию может относиться положение дома на географической карте, то есть его координаты, цвет дома, и так далее, то есть такие показатели, которые для каждого отдельного дома могут быть относительно индивидуальны.
     * 
     * 
     * В качестве интерфейса приспособленца выступает абстрактный класс House, который определяет переменную stages - количество этажей, поскольку количество этажей относится к внутреннему состоянию, которое присуще всем домам. 
     * И также определяется метод Build(), который в качестве параметра принимает широту и долготу расположения дома - внешнее состояние.
     * 
     * Конкретные классы разделяемых приспособленцев - PanelHouse и BrickHouse отвечают за построение конкретных типов домов. 
     * Поскольку архитектурный план проектирования может точно задавать количество этажей для определенного типа дома, то в данном случае количество этажей устанавливается в конструкторе.
     * 
     * Фабрика HouseFactory создает два объекта дома для каждого конкретного приспособленца и возвращает их в методе GetHouse() в зависимости от параметра.
     * 
     * В роли клиента выступает класс Program, который задает начальные широту и долготу - внешнее состояние домов и использует фабрику для создания домов. 
     * Причем в реальности мы будем оперировать всего лишь двумя объектами, которые будут храниться в словаре в HouseFactor
     * */

    class Program
    {
        static void Main(string[] args)
        {
            double longitude = 37.61;
            double latitude = 55.74;

            HouseFactory houseFactory = new HouseFactory();
            for (int i = 0; i < 5; i++)
            {
                House panelHouse = houseFactory.GetHouse("Panel");
                if (panelHouse != null)
                    panelHouse.Build(longitude, latitude);
                longitude += 0.1;
                latitude += 0.1;
            }

            for (int i = 0; i < 5; i++)
            {
                House brickHouse = houseFactory.GetHouse("Brick");
                if (brickHouse != null)
                    brickHouse.Build(longitude, latitude);
                longitude += 0.1;
                latitude += 0.1;
            }

            Console.Read();
        }        
    }


    abstract class House
    {
        protected int stages; // количество этажей

        public abstract void Build(double longitude, double latitude);
    }


    class PanelHouse : House
    {
        public PanelHouse()
        {
            stages = 16;
        }

        public override void Build(double longitude, double latitude)
        {
            Console.WriteLine("Построен панельный дом из 16 этажей; координаты: {0} широты и {1} долготы",
                latitude, longitude);
        }
    }


    class BrickHouse : House
    {
        public BrickHouse()
        {
            stages = 5;
        }

        public override void Build(double longitude, double latitude)
        {
            Console.WriteLine("Построен кирпичный дом из 5 этажей; координаты: {0} широты и {1} долготы",
                latitude, longitude);
        }
    }


    class HouseFactory
    {
        Dictionary<string, House> houses = new Dictionary<string, House>();
        public HouseFactory()
        {
            houses.Add("Panel", new PanelHouse());
            houses.Add("Brick", new BrickHouse());
        }

        public House GetHouse(string key)
        {
            if (houses.ContainsKey(key))
                return houses[key];
            else
                return null;
        }
    }
}
