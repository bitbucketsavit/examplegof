﻿using System;


/*
 * Паттерн Посредник (Mediator) представляет такой шаблон проектирования, 
 * который обеспечивает взаимодействие множества объектов без необходимости ссылаться друг на друга. 
 * Тем самым достигается слабосвязанность взаимодействующих объектов.
 * 
 * 
 * Когда используется паттерн Посредник?
 * 
 * Когда имеется множество взаимосвязаных объектов, связи между которыми сложны и запутаны.
 * 
 * Когда необходимо повторно использовать объект, однако повторное использование затруднено 
 * в силу сильных связей с другими объектами.
 * 
 * 
 * В итоге применение паттерна Посредник дает нам следующие преимущества:
 * 
 * Устраняется сильная связанность между объектами Colleague
 * 
 * Упрощается взаимодействие между объектами: 
 * вместо связей по типу "все-ко-всем" применяется связь "один-ко-всем"
 * 
 * Взаимодействие между объектами абстрагируется и выносится в отдельный интерфейс
 * 
 * Централизуется управления отношениями между объектами
*/

namespace Example.GoF.Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            ManagerMediator mediator = new ManagerMediator();
            Colleague customer = new CustomerColleague(mediator);
            Colleague programmer = new ProgrammerColleague(mediator);
            Colleague tester = new TesterColleague(mediator);
            mediator.Customer = customer;
            mediator.Programmer = programmer;
            mediator.Tester = tester;
            customer.Send("Есть заказ, надо сделать программу");
            programmer.Send("Программа готова, надо протестировать");
            tester.Send("Программа протестирована и готова к продаже");

            Console.Read();
        }
    }


    /// <summary>
    /// Посредник
    /// </summary>
    abstract class Mediator
    {
        public abstract void Send(string msg, Colleague colleague);
    }

    /// <summary>
    /// Абстрактный класс Коллеги
    /// </summary>
    abstract class Colleague
    {
        protected Mediator mediator;

        public Colleague(Mediator mediator)
        {
            this.mediator = mediator;
        }

        public virtual void Send(string message)
        {
            mediator.Send(message, this);
        }

        public abstract void Notify(string message);
    }


    /// <summary>
    /// класс заказчика
    /// </summary>
    class CustomerColleague : Colleague
    {
        public CustomerColleague(Mediator mediator)
            : base(mediator)
        { }

        public override void Notify(string message)
        {
            Console.WriteLine("Сообщение заказчику: " + message);
        }
    }


    /// <summary>
    /// класс программиста
    /// </summary>
    class ProgrammerColleague : Colleague
    {
        public ProgrammerColleague(Mediator mediator)
            : base(mediator)
        { }

        public override void Notify(string message)
        {
            Console.WriteLine("Сообщение программисту: " + message);
        }
    }


    /// <summary>
    /// класс тестера
    /// </summary>
    class TesterColleague : Colleague
    {
        public TesterColleague(Mediator mediator)
            : base(mediator)
        { }

        public override void Notify(string message)
        {
            Console.WriteLine("Сообщение тестеру: " + message);
        }
    }

    /// <summary>
    /// Посредник
    /// </summary>
    class ManagerMediator : Mediator
    {
        public Colleague Customer { get; set; }
        public Colleague Programmer { get; set; }
        public Colleague Tester { get; set; }
        public override void Send(string msg, Colleague colleague)
        {
            // если отправитель - заказчик, значит есть новый заказ
            // отправляем сообщение программисту - выполнить заказ
            if (Customer == colleague)
                Programmer.Notify(msg);

            // если отправитель - программист, то можно приступать к тестированию
            // отправляем сообщение тестеру
            else if (Programmer == colleague)
                Tester.Notify(msg);

            // если отправитель - тест, значит продукт готов
            // отправляем сообщение заказчику
            else if (Tester == colleague)
                Customer.Notify(msg);
        }
    }
}
