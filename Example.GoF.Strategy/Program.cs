﻿using System;

/*
 * Паттерн Стратегия (Strategy) представляет шаблон проектирования, который определяет набор алгоритмов, 
 * инкапсулирует каждый из них и обеспечивает их взаимозаменяемость. 
 * В зависимости от ситуации мы можем легко заменить один используемый алгоритм другим. 
 * При этом замена алгоритма происходит независимо от объекта, который использует данный алгоритм.
 * 
 * 
 * Когда использовать стратегию?
 * 
 * + Когда есть несколько родственных классов, которые отличаются поведением. 
 * Можно задать один основной класс, а разные варианты поведения вынести в отдельные классы и при необходимости их применять
 * 
 * + Когда необходимо обеспечить выбор из нескольких вариантов алгоритмов, которые можно легко менять в зависимости от условий
 * 
 * + Когда необходимо менять поведение объектов на стадии выполнения программы
 * 
 * + Когда класс, применяющий определенную функциональность, ничего не должен знать о ее реализации
*/

namespace Example.GoF.Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            Car auto = new Car(4, "Volvo", new PetrolMove());
            auto.Move();
            auto.Movable = new ElectricMove();
            auto.Move();

            Console.ReadLine();
        }
    }

    abstract class Movable
    {
        public abstract void Move();
    }

    class PetrolMove : Movable
    {
        public override void Move()
        {
            Console.WriteLine("Перемещение на бензине");
        }
    }

    class ElectricMove : Movable
    {
        public override void Move()
        {
            Console.WriteLine("Перемещение на электричестве");
        }
    }

    class Car
    {
        protected int passengers; // кол-во пассажиров
        protected string model; // модель автомобиля

        public Car(int num, string model, Movable mov)
        {
            this.passengers = num;
            this.model = model;
            Movable = mov;
        }
        public Movable Movable { private get; set; }
        public void Move()
        {
            Movable.Move();
        }
    }
}
